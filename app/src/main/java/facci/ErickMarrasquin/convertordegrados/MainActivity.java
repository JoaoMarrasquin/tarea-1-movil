package facci.ErickMarrasquin.convertordegrados;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.content.Intent;
import android.view.View;
import android.widget.Button;

import com.example.convertordegrados.R;

public class MainActivity extends AppCompatActivity {

    Button temperatura= null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        temperatura=(Button)findViewById(R.id.button);
        temperatura.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                Intent cambiar = new Intent(getApplicationContext(),Temperatura.class);
                startActivity(cambiar);
            }
        });
    }
}