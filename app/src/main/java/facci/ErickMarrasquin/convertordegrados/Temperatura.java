package facci.ErickMarrasquin.convertordegrados;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.convertordegrados.R;

public class Temperatura extends AppCompatActivity {

    Button convertir= null;
    EditText cantidad = null;
    TextView resultado = null;
    Spinner spin = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_temperatura);

        convertir=(Button)findViewById(R.id.button2);
        cantidad=(EditText)findViewById(R.id.cantidad);
        resultado=(TextView) findViewById(R.id.resultado);
        spin=(Spinner) findViewById(R.id.spinner);

        String[]op={"Selecciona una opción","°C a °F","°F a °C"};

        ArrayAdapter<String> adapter=new
                ArrayAdapter<String>(this, android.R.layout.simple_spinner_item,op);
        spin.setAdapter(adapter);

        convertir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(cantidad.getText().toString().equals("")){
                    Toast msg=Toast.makeText(getApplicationContext(),"Escribe una cantidad",Toast.LENGTH_LONG);
                    msg.show();
                }else{
                    Double c=Double.parseDouble(cantidad.getText().toString());
                    Double res=null;
                    int select=spin.getSelectedItemPosition();

                    switch (select){
                        case 0:
                            res=0.0;
                            Toast.makeText(getApplicationContext(),"Seleccione una opción",Toast.LENGTH_LONG).show();
                            break;

                        case 1:
                            res=1.8*c+32;
                            break;
                        case 2:
                            res=(c-32)/1.8;
                            break;
                    }
                    resultado.setText(res.toString());
                }
            }
        });
    }
}